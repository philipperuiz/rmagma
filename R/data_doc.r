#' @name HMP
#' @title Data - Structure, function and diversity of the healthy human microbiome (V35)
#'
#' @description
#' This HMP production phase represents pyrosequencing of 16S rRNA genes amplified from multiple body sites across hundreds of human subjects. There are two time points represented for a subset of these subjects. Using default protocol v4.2., data for the 16S window spanning V3-V5 was generated for all samples, with a second 16S window spanning V1-V3 generated for a majority of the samples. 16S rRNA sequencing is being used to characterize the complexity of microbial communities at individual body sites, and to determine whether there is a core microbiome at each site. Several body sites will be studied, including the gastrointestinal and female urogenital tracts, oral cavity, nasal and pharyngeal tract, and skin.
#'
#' Qiita Study ID: 1928
#'
#' Owner: gail.ackerman@colorado.edu
#'
#' PI: Curtis Huttenhower (Harvard University)
#'
#' Publications: doi:10.1038/nature11234, doi:10.1038/nature11234
#'
#' @docType data
#' @usage data("HMP")
#' @format A list with 3 elements:
#' \describe{
#'   \item{otu_table}{Otu abundance table (ID: 3540). Number of samples: 6000. Number of OTUs: 10730 }
#'   \item{row_metadata}{Sample informations. Number of columns: 30}
#'   \item{col_taxonomy}{Taxonomy of each of the OTUs present in otu_table}
#' }
#' @source \url{https://qiita.ucsd.edu/study/description/1928#}
NULL

#' @name bee
#' @title Data - Microbiome of honey bees from Puerto Rico
#'
#' @description
#' Samples of bees, royal jelly from honey bees (Apis mellifera) in Puerto Rico. Includes whole head, whole larva, whole pupa, whole gut and royal jelly. Effect of Tetracycline application.
#'
#' Qiita Study ID: 1064
#'
#' Owner: gail.ackerman@colorado.edu
#'
#' PI: MG Dominguez-Bello (UPR)
#'
#' EBI: ERP016607 (submitted)
#'
#' @docType data
#' @usage data("bee")
#' @format A list with 3 elements
#' #' @format A list with 3 elements:
#' \describe{
#'   \item{otu_table}{Otu abundance table (ID: 2038). Number of samples: 387. Number of OTUs: 3789}
#'   \item{row_metadata}{Sample informations. Number of columns: 65}
#'   \item{col_taxonomy}{Taxonomy of each of the OTUs present in otu_table}
#' }
#' @source \url{https://qiita.ucsd.edu/study/description/1064#}
NULL
