#
####################################################################
####################################################################
#
# MAGMA: inference of sparse microbial association networks
# Arnaud Cougoul, Xavier Bailly and Ernst Wit
# 29/01/2019
# 
# Simulation functions
# and AUC extension for huge::huge.roc
# 
####################################################################
####################################################################
# 


####################################################################
####################################################################
# Simulation function with varying parameters

simulation <- function(seed=NULL, n, d, e=d, distrib="ZINB", graph_topology="erdos_renyi",
                       artefact_rate = 0, cond = 10, X=NULL, Xcoef=NULL, data=NULL, magma_with_cov=F)
{
  print(seed)

  ###################################
  ## Data generation
  grapgen <- magma.gen (n=n, d=d, e=e, data=data, graph_topology=graph_topology, distrib=distrib,
                        seed=seed, cond=cond, artefact_rate=artefact_rate, X=X, Xcoef=Xcoef)
  graph   <- grapgen$graph
  dat_sim <- grapgen$y

  ###################################
  ## Glasso
  n_lambda <- 50
  l_m_e    <- 1e-2
  if (magma_with_cov){
    magma_gl_path <- magma(dat_sim,
                           X = X,
                           method = "glasso",
                           lambda.min.ratio = l_m_e,
                           nlambda = n_lambda,
                           verbose = F,
                           magma.select = F)$path
  } else {
    magma_gl_path <- magma(dat_sim,
                           method = "glasso",
                           lambda.min.ratio = l_m_e,
                           nlambda = n_lambda,
                           verbose = F,
                           magma.select = F)$path
  }

  # ROC
  roc_magma_gl <- huge::huge.roc(magma_gl_path, graph, verbose=F)

  # AUC
  AUC_extension_trapeze(roc_magma_gl)
}


####################################################################
####################################################################
# Simulation function to compare methods

method_bench <- function(seed, distrib="ZINB", graph_topology="erdos_renyi",
                         n, d, e=d, artefact_rate = 0, cond = 10,
                         X=NULL, Xcoef=NULL, data=NULL, magma_with_cov=F)
{
  print(seed)

  ###################################
  ## Data generation
  grapgen <- magma.gen (n=n, d=d, e=e, data=data, graph_topology=graph_topology, distrib=distrib,
                        seed=seed, cond=cond, artefact_rate=artefact_rate, X=X, Xcoef=Xcoef)
  graph   <- grapgen$graph
  dat_sim <- grapgen$y

  ###################################
  ## Glasso parameters
  n_lambda <- 100
  l_m_e    <- 1e-2

  cat("SE_gl ")
  T1<-Sys.time()
  SE_gl_path <- spiec.easi(dat_sim, method="glasso",pulsar.select=FALSE, lambda.min.ratio=l_m_e, nlambda=n_lambda,verbose = F)$est$path
  T2<-Sys.time()

  cat(difftime(T2,T1, units="secs")," ,sparCC ")
  sparCC_cor <- sparcc(dat_sim, iter = 100, inner_iter = 20, th = 0.1)$Cor
  T1<-Sys.time()

  cat(difftime(T1,T2, units="secs")," ,npn_gl ")
  dat_skept <- huge.npn(dat_sim, npn.func = "skeptic",verbose = F)

  npn_gl_path <- huge(dat_skept, lambda.min.ratio=l_m_e, nlambda=n_lambda,verbose = F)$path
  T2<-Sys.time()

  cat(difftime(T2,T1, units="secs")," ,raw_gl ")
  raw_gl_path <- huge(dat_sim, method="glasso", lambda.min.ratio=l_m_e, nlambda=n_lambda,verbose = F)$path
  T1<-Sys.time()

  cat(difftime(T1,T2, units="secs")," ,magma_gl ")
  if (!magma_with_cov){X=NULL}
  magma_gl_path <- magma(dat_sim,X=X,method="glasso",lambda.min.ratio=l_m_e,nlambda=n_lambda,verbose=F,magma.select=F)$path
  T2<-Sys.time()

  cat(difftime(T2,T1, units="secs")," ,conet ")
  conet_pval <- getNetwork(t(dat_sim), method="spearman", T.up=0.2, T.down=-0.2, iters=100, report.full=T, verbose=F, permutandboot = T, permut = T)$pvalues
  T1<-Sys.time()

  cat(difftime(T1,T2, units="secs")," ,roc ", "\n")

  ## path for sparCC, Spearman and Pearson
  # correlations below the threshold are fixed to 0
  sparCC_cor  <- abs(sparCC_cor); diag(sparCC_cor) <- 0
  pearson_cor <- abs(cor(dat_sim,method = "pearson")); diag(pearson_cor) <- 0
  spearman_cor <- abs(cor(dat_sim,method = "spearman")); diag(spearman_cor) <- 0

  threshold <- seq(1,0,-1/(2*n_lambda))
  sparCC_path <- pearson_path <- spearman_path <- list()
  for (k in 1:length(threshold)) {
    sparCC_path[[k]]   <- (sparCC_cor >= threshold[k]) * 1
    pearson_path[[k]]  <- (pearson_cor >= threshold[k]) * 1
    spearman_path[[k]] <- (spearman_cor >= threshold[k]) * 1
  }

  ## path for conet
  # pvalue > threshold fixed to 0
  rrr<-range(conet_pval, na.rm=T)
  threshold_conet <- c(0,exp(seq(log(max(rrr[1],1e-8)),log(min(rrr[2],1)),length.out = 2*n_lambda)))
  conet_path <- list()
  for (k in 1:length(threshold_conet)) {
    conet_path[[k]] <- (conet_pval <= threshold_conet[k]) * 1
  }


  # ROC
  roc_pearson  <- huge.roc(pearson_path, graph, verbose=F)
  roc_spearman <- huge.roc(spearman_path, graph, verbose=F)
  roc_raw_gl   <- huge.roc(raw_gl_path, graph, verbose=F)
  roc_npn_gl   <- huge.roc(npn_gl_path, graph, verbose=F)
  roc_SE_gl    <- huge.roc(SE_gl_path, graph, verbose=F)
  roc_sparCC   <- huge.roc(sparCC_path, graph, verbose=F)
  roc_magma_gl <- huge.roc(magma_gl_path, graph, verbose=F)
  roc_conet    <- huge.roc(conet_path, graph, verbose=F)

  # AUC
  rez <- c(
    pearson  = AUC_extension_trapeze(roc_pearson),
    spearman = AUC_extension_trapeze(roc_spearman),
    raw_gl   = AUC_extension_trapeze(roc_raw_gl),
    npn_gl   = AUC_extension_trapeze(roc_npn_gl),
    sparCC   = AUC_extension_trapeze(roc_sparCC),
    SE_gl    = AUC_extension_trapeze(roc_SE_gl),
    magma_gl = AUC_extension_trapeze(roc_magma_gl),
    conet    = AUC_extension_trapeze(roc_conet)
    )
  print (rez)
  rez
}


####################################################################
####################################################################
# AUC extension

# trapeze extension of AUC
AUC_extension_trapeze <- function (res.huge.roc)
{
  res.huge.roc$AUC + (max(res.huge.roc$tp)+1)*(1-max(res.huge.roc$fp))/2
}

# rectangular extension of AUC
AUC_extension_rectangular <- function (res.huge.roc)
{
  res.huge.roc$AUC + max(res.huge.roc$tp)*(1-max(res.huge.roc$fp))
}


